@extends('index')

@section('page-css')
    <link href="{{ asset('css/statistics.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
@endsection

@section('content')
    <div class="main container-lg mt-5">
        <div class="title-row" style="width: 100%">
            <div class='big-title'>Statistics</div>
            <p class="line"></p>
        </div>

        <div class="statistic-content mt-5">
            <div class="statistic-nav">
                <div class="nav-title">
                    Navigation
                </div>
                <div class="nav-list mt-4">
                    <a class="list-item oval"
                       href="#spendings-bar"
                       style="background-color: #F8CFD5; box-shadow: 7px 7px 10px 0px #F8CFD5;">
                        Spendings bar
                    </a>
                    <a class="list-item oval"
                       href="#income-bar"
                       style="background-color: #FDE7AD; box-shadow: 7px 7px 10px 0px #FDE7AD;">
                        Income bar
                    </a>
                    <a class="list-item oval"
                       href="#income-and-spendings-graph"
                       style="background-color: #A1E5E0; box-shadow: 7px 7px 10px 0px #A1E5E0;">
                        Income & spendings graph
                    </a>

                    <div class="period mt-4">
                        <div class="period-title d-flex align-items-center">
                            <div class="title" style="margin-right: 10px">Period</div>
                            <p class="line"></p>
                        </div>
                        <a class="oval list-item mt-4" href="{{ route('statistics-page', ['period' => 'week']) }}">
                            week
                        </a>
                        <a class="oval list-item mt-2" href="{{ route('statistics-page', ['period' => 'month']) }}">
                            month
                        </a>
                        <a class="oval list-item mt-2" href="{{ route('statistics-page', ['period' => 'year']) }}">
                            year
                        </a>
                    </div>

                </div>
            </div>
            <div class="statistic-charts">
                <div class="chart-content">
                    <div class="chart-title">
                        <div id="spendings-bar">Spendings bar</div>
                        <p class="line" style="width: 10%"></p>
                    </div>
                    <div class="chart-data mt-4">
                        <canvas id="spendingsBar" style="width:100%;max-width:1000px"></canvas>
                    </div>
                </div>
                <div class="chart-content mt-5">
                    <div class="chart-title">
                        <div id="income-bar">Income bar</div>
                        <p class="line" style="width: 10%"></p>
                    </div>
                    <div class="chart-data mt-4">
                        <canvas id="incomeBar" style="width:100%;max-width:1000px"></canvas>
                    </div>
                </div>
                <div class="chart-content mt-5">
                    <div class="chart-title">
                        <div id="income-and-spendings-graph"> Income & spendings graph</div>
                        <p class="line" style="width: 10%"></p>
                    </div>
                    <div class="chart-data mt-4">
                        <canvas id="incomeAndSpendingsGraph" style="width:100%;max-width:1000px"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        const xValues = {!! $periods !!};
        const yValues = {!! $valuesSpending !!};
        const barColors = {!! $colorsSpending !!};

        new Chart("spendingsBar", {
            type: "bar",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
            }
        });


        const xValuesIncome = {!! $periods !!};
        const yValuesIncome = {!! $valuesIncome !!};
        const barColorsIncome = {!! $colorsIncome !!};

        new Chart("incomeBar", {
            type: "bar",
            data: {
                labels: xValuesIncome,
                datasets: [{
                    backgroundColor: barColorsIncome,
                    data: yValuesIncome
                }]
            },
            options: {
                legend: {
                    display: false
                },
            }
        });

        const xGraphValues = {!! $periods !!};

        new Chart("incomeAndSpendingsGraph", {
            type: "line",
            data: {
                labels: xGraphValues,
                datasets: [{
                    data: yValues,
                    borderColor: "#EC7063",
                    fill: false
                },{
                    data: yValuesIncome,
                    borderColor: "#4CB45A",
                    fill: false
                },
                ]
            },
            options: {
                legend: {display: false}
            }
        });
    </script>

@endsection

