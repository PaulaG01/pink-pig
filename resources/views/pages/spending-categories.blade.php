@extends('index')

@section('page-css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vertical_menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/accounts.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
    <link href="{{ asset('css/categories_spendings_wheel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/create_account.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
@endsection

@section('content')
    <div class="main container-lg mt-5">
        <x-blocks.vertical-menu/>
        <div class="right-container" style="width: 80%">
            <div class="accounts_row">
                <div class="title-row">
                    <div class='big-title'>Spending categories</div>
                    <p class="line"></p>
                </div>
                <div class="info mt-3">
                    <x-blocks.categories-spendings-wheel
                        :spendingCategories="$spendingCategories"
                        :wheelColors="$wheelColors"
                        :wheelPercentages="$wheelPercentages"
                        :isNeedPeriods="false"
                        :periodStart="\Carbon\Carbon::now()->startOfMonth()"
                        :periodEnd="\Carbon\Carbon::now()->endOfMonth()"
                        :addCreateButton="true"
                    />
                </div>
            </div>
        </div>
    </div>
@endsection
