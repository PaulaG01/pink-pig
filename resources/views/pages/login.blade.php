@extends('index')

@section('page-css')
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="main container-lg mt-5">
        <div class="login-box">
            <div class="login-form">
                <div class="form-item form-active">
                    <form action="{{ route('login') }}" method="POST" class="login">
                        @csrf
                        <h2 class="title">
                            LOGIN
                        </h2>
                        <div class="login__field">
                            <input type="text" class="login__input" placeholder="e-mail" name="email">
                        </div>
                        <div class="login__field">
                            <input type="password" class="login__input" placeholder="password" name="password">
                        </div>
                        @if($errors->any())
                            <div class="errors">
                                @foreach ($errors->all() as $error)
                                    <p class="text-danger"> {{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <button class="oval">
                            <span class="button__text">Login</span>
                        </button>

                        <div class="change-form mt-3">
                            <div>Don't have account?</div>
                            <a href="{{ route('register-page') }}">Sign up now!</a>
                        </div>
                    </form>

                    <div class="form-name">
                        <div>login</div>
                    </div>

                </div>
                <div class="form-item form-back" onclick="changeForm()">
                    <form action="{{ route('login') }}" method="POST" class="login">
                    </form>

                    <div class="form-name" id='register_text'>
                        <div>register</div>
                    </div>
                </div>
            </div>
            <div class="login-image">
                <img src="{{ asset('images/items/blue-pig.jfif') }}" alt="blue-pig">
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/login.js') }}"></script>
@endsection
