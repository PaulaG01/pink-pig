@extends('index')

@section('page-css')
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
    <link href="{{ asset('css/profile.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="main container-lg mt-5 profile-container">
        <div class="user-profile">
            <div class="title-row" style="width: 100%">
                <div class='big-title'>Profile</div>
                <p class="line"></p>
            </div>
            <div class="user-data mt-5">
                <form action="{{ route('users.update') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $user->id }}" name="id">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control profile-input" id="name" name="name" placeholder="name"
                            value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <label for="email" class="col-sm-2 col-form-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control profile-input" id="email" name="email" placeholder="email"
                            value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <label for="created_at" class="col-sm-2 col-form-label">Created at</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control profile-input" id="created_at" name="created_at"
                            value="{{ \Carbon\Carbon::make($user->created_at)->format('Y-m-d/m/y H:i') }}" disabled>
                        </div>
                    </div>
                    <div class="form-footer mt-4 d-flex justify-content-center">
                        <button class="oval">
                            save
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="profile-image">
            <img src="{{ asset('images/items/flower-coins.png') }}" alt="">
        </div>
    </div>
@endsection
