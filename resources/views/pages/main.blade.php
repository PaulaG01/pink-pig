@extends('index')

@section('page-css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vertical_menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/categories_spendings_wheel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/last_transactions.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
    <link href="{{ asset('css/create_account.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
@endsection

@section('content')
    <div class="main container-lg mt-5">
        <x-blocks.vertical-menu/>
        <x-blocks.categories-spendings-wheel
            :spendingCategories="$spendingCategories"
            :period="$period"
            :periodStart="$periodStart"
            :periodEnd="$periodEnd"
            :wheelColors="$wheelColors"
            :wheelPercentages="$wheelPercentages"
        />
        <x-blocks.transactions-block
            :lastTransactions="$lastTransactions"
        />
    </div>
    <x-layouts.footer
        :mainAccountBalance="$sources->where('is_main')->first()->getBalance('byn')"
        :totalAccountsBalance="$totalAccountsBalance"
        :totalSpent="$totalSpent"
        :moreThanLastMonth="$moreThanLastMonth"
    />
    <x-popups.createTransactionModal
        :sources="$sources"
        :targets="$targets"
    />
@endsection

<script>
    function openCreateModal() {
        let modal = document.querySelector(`[data-id="create-transactions-modal"]`);
        modal.style.display = "block";

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
</script>
