@extends('index')

@section('page-css')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vertical_menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/accounts.css') }}" rel="stylesheet">
    <link href="{{ asset('css/account_card.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
    <link href="{{ asset('css/create_account.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="main container-lg mt-5">
        <x-blocks.vertical-menu/>
        <div class="left-container">
            <p class='big-title'>Details</p>
            <div class="details_block">
                <div class="title-row mb-3">
                    <div class="title">Cards and accounts</div>
                    <p class="line"></p>
                </div>
                <div class="text_row">
                    Accounts count: {{ $totalCount }}
                </div>
                <div class="text_row">
                    Total amount: {{ $totalAmount }} byn
                </div>
{{--                <div class="text_row">--}}
{{--                    Transactions count:--}}
{{--                </div>--}}
{{--                <div class="text_row">--}}
{{--                    Total income:--}}
{{--                </div>--}}
{{--                <div class="text_row">--}}
{{--                    Total spent:--}}
{{--                </div>--}}
            </div>

            <div class="details_block">
                <div class="title-row mb-3">
                    <div class="title">Savings</div>
                    <p class="line"></p>
                </div>
                <div class="text_row">
                    Accounts count: {{ $totalSavingsCount }}
                </div>
                <div class="text_row">
                    Total amount: {{ $totalSavingsAmount }} byn
                </div>
            </div>

            <div class="create-account">
                <div class="oval" id="createAccountPopup" onclick="openCreateModal()">
                    create
                </div>
            </div>
        </div>
        <div class="right-container">
            <div class="accounts_row">
                <div class="title-row">
                    <div class='big-title'>Income accounts</div>
                    <p class="line"></p>
                </div>

                <div class="accounts-list mt-3">
                    @foreach($incomes as $income)
                        <div class="income d-flex flex-column align-items-center" style="gap: 10px; margin-right: 10px">
                            <div class="circle" style="background-color: {{ $income->color->hash }}">
                                IN
                            </div>
                            <span class="mt-1"> {{ $income->name }}</span>
                        </div>
                    @endforeach
                </div>

            </div>
            <div class="accounts_row mt-3">
                <div class="title-row">
                    <div class='big-title'>CARDS AND ACCOUNTS</div>
                    <p class="line"></p>
                </div>

                <div class="accounts-list mt-3">
                    @foreach($accounts as $account)
                        <x-blocks.account-card
                            :account="$account"
                            :accountCategories="$accountCategories"
                            :currencies="$currencies"
                        />
                    @endforeach
                </div>

            </div>
            <div class="accounts_row mt-3">
                <div class="title-row">
                    <div class='big-title'>SAVINGS</div>
                    <p class="line"></p>
                </div>

                <div class="accounts-list mt-3">
                    @foreach($savings as $account)
                        <x-blocks.account-card
                            :account="$account"
                            :accountCategories="$accountCategories"
                            :currencies="$currencies"
                        />
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <x-popups.createAccountModal
        :accountCategories="$accountCategories"
        :currencies="$currencies"
    />
@endsection

<script>
    function openCreateModal() {
        let modal = document.querySelector(`[data-id="0"]`);
        modal.style.display = "block";

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
</script>
