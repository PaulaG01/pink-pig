@extends('index')

@section('page-css')
    <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/oval.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="welcome container-lg mt-5">
        <div class="greeting">
            <h1>Let us help you to know where your money are</h1>
        </div>
        <div class="content">
            <div class="coins top">
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
            </div>
            <div class="coins bot">
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
                <div class="coin">
                    <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
                </div>
            </div>
            <div class="advantages mt-4">
                <div class="advantage oval color-dark-red">
                    fast
                </div>
                <div class="advantage oval color-gold">
                    safe
                </div>
                <div class="advantage oval color-light-green">
                    easy
                </div>
                <div class="advantage oval color-dark-red">
                    helpful
                </div>
                <div class="advantage oval color-gold">
                    cute
                </div>
            </div>
        </div>
    </div>
@endsection
