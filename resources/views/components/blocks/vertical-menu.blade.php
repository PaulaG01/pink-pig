<div class="vertical-menu">
    <div class="vertical-menu-block">
        <div class="title">
            Finance
        </div>
        <div class="vertical-items">
            <a class="item" href="{{ route('accounts-page') }}">
                Accounts
            </a>
            <a class="item" href="{{ route('spending-categories.page') }}">
                Categories
            </a>
            <a class="item" href="{{ route('statistics-page') }}">
                Statistics
            </a>
        </div>
    </div>
    <div class="vertical-menu-block">
        <div class="title">
            Income
        </div>
        <div class="vertical-items">
            <div class="item">
                Income
            </div>
            <div class="item">
                Statistics
            </div>
            <div class="item">
                Settings
            </div>
        </div>
    </div>
    <div class="vertical-menu-block">
        <div class="title">
            Models
        </div>
        <div class="items">
            <div class="item">
                50/30/20
            </div>
        </div>
    </div>
</div>
