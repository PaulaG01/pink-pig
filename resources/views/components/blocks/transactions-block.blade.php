<div class="last-transactions">
    <div class="title">
        Last transactions
    </div>
    <div class="history">
        @foreach($lastTransactions as $transaction)
            <div class="item">
                <div class="account">
                    {{ $transaction->source()->name }}
                </div>
                <div class="arrow-right">
                    &#x27F6;
                </div>
                <div class="account">
                    {{ $transaction->target()->name }}
                </div>
                <div class="details">
                    <div class="amount">
                        <span class="color-gold">{{ $transaction->amount }} </span><span class="small">byn</span>
                    </div>
                    <div class="date small">
                        {{ \Carbon\Carbon::make($transaction->created_at)->format('d.m.Y') }}
                    </div>
                </div>
            </div>
        @endforeach
        <div class="add-transaction">
            <button class="oval" onclick="openCreateModal()">
                add
            </button>
        </div>
    </div>
</div>

<script>
    function openCreateModal() {
        let modal = document.getElementById('modal');
        modal.style.display = "block";

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
</script>
