<div class="card {{ $account->is_main ? 'main-account' : '' }}" onclick="openModal({{ $account->id }})">
    <div class="card_head">
        <div class="card_title">
            {{ $account->name }}
            @if($account->is_main)
                <span class="color-gold">(main)</span>
            @endif
        </div>

        <div class="circle" style="background-color: {{ $account->color->hash }}">
            {{ $account->shortName }}
        </div>
    </div>
    <div class="card_data">
        <div class="data-amount">
            {{ $account->balance }}
            <span class="currency-value">{{ $account->currency->symbol }}</span>
        </div>
        <div class="data-converted">
            @if($account->currency->symbol !== 'usd')
                <div class="val">
                    {{ $account->usd }} USD
                </div>
            @endif
            @if($account->currency->symbol !== 'eur')
                <div class="val">
                    {{ $account->eur }} EUR
                </div>
            @endif
            @if($account->currency->symbol !== 'byn')
                <div class="val">
                    {{ $account->byn }} BYN
                </div>
            @endif
        </div>
    </div>
    <div class="card_footer">
        <a class="transactions_link" href="{{ $account->linkToTransactions }}">
            view transactions
        </a>
    </div>
</div>

<x-popups.accountModal
    :account="$account"
/>

<script>
    function openModal(id = 0) {
        let modal = document.querySelector(`[data-id="${id}"]`);
        modal.style.display = "block";

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
</script>
