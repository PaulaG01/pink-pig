<div class="spendings-wheel">
    <div class="statistic">
        <div class="wheel">
            <canvas id="myChart" style="width:380px!important; height: 100%!important;"></canvas>
        </div>
        <div class="spendings">
            <div class="header">
                <div class="title">
                    Categories
                </div>
                <div class="period">
                    {{ \Carbon\Carbon::make($periodStart)->format('d M Y') }}
                    - {{ \Carbon\Carbon::make($periodEnd)->format('d M Y') }}
                </div>
            </div>
            <div class="categories" @if(!$isNeedPeriods)
                style="display: flex; flex-wrap: wrap"
                @endif>
                @foreach($spendingCategories as $spendingCategory)
                    <div class="item">
                        <div class="circle"
                             style="background-color: {{ $spendingCategory->bg_color }}">
                            <img src="{{ asset($spendingCategory->icon_path) }}" alt="">
                        </div>
                        <div class="title">
                            {{ $spendingCategory->name }}
                        </div>
                        <div class="spent">
                            <div class="amount">
                                <span class="color-gold">{{ $spendingCategory->amount }}</span><span
                                    class="small"> byn</span>
                            </div>
                            <div class="percent small">
                                {{ $spendingCategory->percentage }}%
                            </div>
                        </div>
                    </div>
                @endforeach
                @if($addCreateButton)
                        <div class="item">
                            <div class="circle circle-create"
                                 style="background-color: #92B679"
                                 onclick="openCreateModal()">
                                <img src="{{ asset('images/icons/plus.png') }}" alt="">
                            </div>
                            <div class="title">
                                Add
                            </div>
                        </div>
                @endif
            </div>
        </div>
    </div>
    @if($isNeedPeriods)
        <div class="periods">
            <a class="item color-dark-red oval {{ $period === 'month' ? 'active' : ''}}"
               href="{{ route('default-with-auth', ['period' => 'month']) }}">
                month
            </a>
            <a class="item color-gold oval {{ $period === 'year' ? 'active' : '' }}"
               href="{{ route('default-with-auth', ['period' => 'year']) }}">
                year
            </a>
            <a class="item color-light-green oval {{ $period === 'week' ? 'active' : '' }}"
               href="{{ route('default-with-auth', ['period' => 'week']) }}">
                week
            </a>
        </div>
    @endif

    @if($addCreateButton)
        <x-popups.createSpendingCategoryModal/>
    @endif
</div>


<script>
    const xValues = {!! $categoriesNames !!};
    let yValues = {!! $wheelPercentages !!};

    const barColors = {!! $wheelColors !!};

    new Chart("myChart", {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            legend: {
                display: false
            },
        }
    });

@if($addCreateButton)
    function openCreateModal() {
        let modal = document.querySelector(`[data-id="create-sc-modal"]`);
        modal.style.display = "block";

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
@endif

</script>
