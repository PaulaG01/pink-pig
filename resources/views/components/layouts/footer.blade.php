<div class="footer">
    <div class="block finance">
        <div class="header">
            <div class="title">
                Finance
            </div>
            <div class="date">
                {{ \Carbon\Carbon::now()->format('d M Y') }}
            </div>
        </div>
        <div class="amount">
            {{ $mainAccountBalance }} <span class="footer-block-currency">byn</span>
        </div>
        <div class="total-amount">
            <div class="value">
                {{ $totalAccountsBalance }} byn
            </div>
            <div class="description">
                total accounts amount
            </div>
        </div>
    </div>
    <div class="block total-spent">
        <div class="header">
            <div class="title">
                Total spent
            </div>
            <div class="date">
                {{ \Carbon\Carbon::now()->format('d M Y') }}
            </div>
        </div>
        <div class="amount">
            {{ $totalSpent }} <span class="footer-block-currency">byn</span>
        </div>
        <div class="total-amount">
            <div class="description">
                @if($moreThanLastMonth > 0)
                    more than last month on <span>{{ $moreThanLastMonth }} byn</span>
                @else
                    less than last month on <span>{{ $moreThanLastMonth }} byn</span>
                @endif

            </div>
        </div>
    </div>
    <div class="block currency">
        <div class="header">
            <div class="title">
                Currencies
            </div>
            <div class="coin">
                <img src="{{ asset('images/coins/pig.png') }}" alt="pig coin">
            </div>
        </div>
        <div class="currencies">
            <table>
                <tr>
                    <th></th>
                    <th>Buy</th>
                    <th>Sell</th>
                </tr>
                <tr class="currency-item">
                    <td>
                        1 USD
                    </td>
                    <td>
                        3,2001
                    </td>
                    <td>
                        3,3200
                    </td>
                </tr>
                <tr class="currency-item">
                    <td>
                        1 EUR
                    </td>
                    <td>
                        3,6592
                    </td>
                    <td>
                        3,7843
                    </td>
                </tr>
                <tr class="currency-item">
                    <td>
                        100 RUB
                    </td>
                    <td>
                        3,3920
                    </td>
                    <td>
                        3,4120
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

@yield('page-script')
</body>
</html>
