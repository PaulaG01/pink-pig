<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PinkPig</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Space+Mono&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    @yield('page-css')
</head>
<body>

<div class="container header">
    <div class="navbar">
        <a class="logo-title" href="{{ Auth::user() ? route('default-with-auth'): route('default-no-auth') }}">
            P P G
        </a>
        <div class="menu">
            <div class="menu-item">
                Rates converter
            </div>
            <div class="menu-item">
                Currencies
            </div>
            <div class="menu-item">
                About app
            </div>
            @if(Auth::user())
                <a class="menu-item profile-header" href="{{ route('profile-page') }}">
                    Profile
                </a>
            @else
                <a class="menu-item profile-header" href="{{ route('login-page') }}">
                    Login
                </a>
            @endif
        </div>
    </div>

</div>
