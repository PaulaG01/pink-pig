<div id="modal" class="modal" data-id="create-transactions-modal">
    <div class="modal-content">
        <span class="close" onclick="closeModal()">&times;</span>
        <div class="modal-container">
            <div class="modal-header">
                <div class="modal__title big-title">
                    Transaction creating
                </div>
            </div>
            <div class="modal__content mt-4 mb-4">
                <form action="{{ route('transactions.create') }}" method="POST">
                    @csrf
                    <fieldset class="form-group mt-4">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Source</legend>
                            <div class="col-sm-10 items">
                                @foreach($sources as $source)
                                    <div class="form-check d-flex mt-2">
                                        <input class="form-check-input d-none" type="radio" name="source_id"
                                               id="source_{{ $source->id }}"
                                               value="{{ json_encode(['id' => $source->id, 'type' => $source::class]) }}">
                                        <label class="form-check-label data" for="source_{{ $source->id }}">
                                            <div class="circle" style="background-color: {{ $source->color->hash }}">
                                                {{ $source->shortName }}
                                            </div>
                                            <div class="name">
                                                {{ $source->name }}
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group mt-4">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Target</legend>
                            <div class="col-sm-10 items">
                                @foreach($targets as $target)
                                    <div class="form-check d-flex mt-2">
                                        <input class="form-check-input d-none" type="radio" name="target_id"
                                               id="target_{{ $target->id }}"
                                               value="{{ json_encode(['id' => $target->id, 'type' => $target::class]) }}">
                                        <label class="form-check-label data" for="target_{{ $target->id }}">
                                            <div class="circle"
                                                 style="background-color: {{ $target->color->hash }}">
                                                @if($target->icon)
                                                    <img src="{{ asset($target->icon->path) }}" alt="">
                                                @else
                                                    {{ $target->shortName }}
                                                @endif
                                            </div>
                                            <div class="name">
                                                {{ $target->name }}
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row mt-4">
                        <label for="amount" class="col-sm-2 col-form-label">Amount</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="amount" name="amount" placeholder="amount">
                        </div>
                    </div>
                    <div class="form-footer mt-4">
                        <button class="oval">
                            save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function closeModal() {
        let modal = document.getElementById('modal');

        modal.style.display = "none";
    }
</script>
