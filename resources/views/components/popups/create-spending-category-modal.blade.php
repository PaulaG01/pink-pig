<div id="modal" class="modal" data-id="create-sc-modal">
    <div class="modal-content">
        <span class="close" onclick="closeModal()">&times;</span>
        <div class="modal-container">
            <div class="modal-header">
                <div class="modal__title big-title">
                    Transaction creating
                </div>
            </div>
            <div class="modal__content mt-4 mb-4">
                <form action="{{ route('spending-categories.create') }}" method="POST">
                    @csrf
                    <div class="form-group row mt-4">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="name">
                        </div>
                    </div>
                    <fieldset class="form-group mt-4">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Color</legend>
                            <div class="col-sm-10 items d-flex align-items-center">
                                @foreach($colors as $color)
                                    <div class="form-check d-flex mt-2">
                                        <input class="form-check-input circle-selected d-none" type="radio" name="color_id"
                                               id="color_{{ $color->id }}"
                                               value="{{ $color->id }}">
                                        <label class="form-check-label data" for="color_{{ $color->id }}">
                                            <div class="circle" style="background-color: {{ $color->hash }}">
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group mt-4">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Icon</legend>
                            <div class="col-sm-10 items d-flex align-items-center">
                                @foreach($icons as $icon)
                                    <div class="form-check d-flex mt-2">
                                        <input class="form-check-input circle-selected d-none" type="radio" name="icon_id"
                                               id="icon_{{ $icon->id }}"
                                               value="{{ $icon->id }}">
                                        <label class="form-check-label data" for="icon_{{ $icon->id }}">
                                            <div class="circle">
                                                <img src="{{ asset($icon->path) }}" alt="">
                                            </div>
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-footer mt-4">
                        <button class="oval">
                            save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function closeModal() {
        let modal = document.getElementById('modal');

        modal.style.display = "none";
    }
</script>
