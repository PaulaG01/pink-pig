<div data-id="{{ isset($account) ? $account->id : 0 }}" class="modal">
    <div class="modal-content">
        <span class="close" onclick="closeModal({{isset($account) ? $account->id : 0}})">&times;</span>
        <div class="modal-container">
            <div class="modal-header">
                <div class="modal__title big-title">
                    Account {{ isset($account) ? 'info' : 'creating' }}
                </div>
            </div>
            <div class="modal__content mt-4 mb-4">
                <form action="{{ isset($account) ? route('accounts.edit') : route('accounts.create') }}"
                      method="POST">
                    @csrf
                    @if(isset($account))
                        <input type="hidden" name="id" value="{{ $account->id }}">
                    @endif
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="name"
                                   value="{{ isset($account) ? $account->name : '' }}">
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <label for="balance" class="col-sm-2 col-form-label">Balance</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="balance" name="balance" placeholder="balance"
                                   value="{{ isset($account) ? $account->balance : '' }}">
                        </div>
                    </div>
                    <fieldset class="form-group mt-4">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Category</legend>
                            <div class="col-sm-10">
                                @foreach($accountCategories as $category)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="account_category_id"
                                               id="gridRadios1"
                                               value="{{ $category->id }}" {{ isset($account) ? ($account->account_category_id === $category->id ? 'checked': '') : '' }}>
                                        <label class="form-check-label" for="gridRadios1">
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row mt-4">
                        <label for="currency_id" class="col-sm-2 col-form-label">Currency</label>
                        <div class="col-sm-10">
                            <select class="form-control col-md-4" required name="currency_id" id="currency_id">
                                @foreach($currencies as $currency)
                                    <option
                                        value="{{ $currency->id }}" {{ isset($account) ? ($account->currency_id === $currency->id ? 'selected': '') : '' }}>
                                        {{ $currency->name }}
                                        ({{ $currency->symbol }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-4">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox"
                                   value="1" id="is_main"
                                   name="is_main"
                                   @if(isset($account) && $account->is_main) checked 11 @endif>
                            <label class="form-check-label" for="is_main">
                                Is main account
                            </label>
                        </div>
                    </div>
                    <div class="form-footer mt-4">
                        <button class="oval">
                            save
                        </button>
                    </div>
                </form>
                @if(isset($account))
                    <div class="account-info mt-4">
                        <div class="info-title">
                            Results for {{ \Carbon\Carbon::now()->monthName }}
                        </div>
                        <div class="month-results">
                            <div class="results">
                                <div class="results__header">
                                    <div class="circle"
                                         style="background-color: var(--text-light-green-color); width: 20px; height: 20px;"></div>
                                    <div class="results__title">Total income</div>
                                </div>
                                <div class="results__total">
                                    {{ $account->income() }} {{ $account->currency->symbol }}
                                </div>
                            </div>
                            <div class="results">
                                <div class="results__header">
                                    <div class="circle"
                                         style="background-color: var(--text-dark-red-color); width: 20px; height: 20px;"></div>
                                    <div class="results__title">Total spent</div>
                                </div>
                                <div class="results__total">
                                    {{ $account->spent() }} {{ $account->currency->symbol }}
                                </div>
                            </div>
                        </div>
                        <div class="last-transactions">
                            @foreach($lastTransactions as $dateString => $transactions)
                                <div class="day-block">
                                    <div class="date">
                                        {{ \Carbon\Carbon::make($dateString)->format('d M, D') }}
                                    </div>
                                    @foreach($transactions as $transaction)
                                        <div class="transaction">
                                            <div class="senders">
                                                <div class="target__icon">
                                                    <div class="target__icon circle"
                                                         style="background-color: {{ $transaction->target()->color->hash }}">
                                                        @if($transaction->target() instanceof \App\Models\Account)
                                                            {{ $transaction->target()->shortName }}
                                                        @else
                                                            <img src="{{ asset($transaction->target()->icon->path) }}"
                                                                 alt="">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="names">
                                                    <div class="target__name">
                                                        {{ $transaction->target()->name }}
                                                    </div>
                                                    <div class="source_name">
                                                        {{ $transaction->source()->name }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="balance">
                                                <div class="transaction__amount">
                                                    {{ $transaction->amount }} {{ $transaction->source()->currency->symbol }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    function closeModal(id = 0) {
        let modal = document.querySelector(`[data-id="${id}"]`);

        modal.style.display = "none";
    }
</script>
