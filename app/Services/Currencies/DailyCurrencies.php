<?php

namespace App\Services\Currencies;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class DailyCurrencies
{
    public static function getFor(string $source = 'byn', $target = 'usd')
    {
        $dailyCurrency = DB::table('daily_currencies')
            ->where('source', $source)
            ->where('target', $target)
            ->first();

        if (!$dailyCurrency) {
            return (object) [
                'source' => $source,
                'target' => $target,
                'currency' => 1,
                'currency_sell' => 1,
                'currency_buy' => 1,
            ];
        }

        $isNeedUpdate = !Carbon::make($dailyCurrency->updated_at)->isToday();
        //update

        return $dailyCurrency;
    }
}
