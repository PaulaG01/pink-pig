<?php

namespace App\Http\Controllers;

use App\Models\SpendingCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpendingCategoriesController extends Controller
{
    public function index()
    {
        $periodStart = Carbon::now()->startOfMonth();
        $periodEnd = Carbon::now()->endOfMonth();
        $spendingCategories = SpendingCategory::mostPopularCategoriesForPeriod(null, $periodStart, $periodEnd, false);
        $wheelColors = $spendingCategories->pluck('bg_color');
        $wheelPercentages = $spendingCategories->pluck('percentage');

        $data = compact(
            'spendingCategories',
            'wheelColors',
            'wheelPercentages',
        );
        return view('pages.spending-categories', $data);
    }

    public function create(Request $request)
    {
        SpendingCategory::query()->create([
            'name' => $request->get('name'),
            'color_id' => $request->get('color_id'),
            'icon_id' => $request->get('icon_id'),
            'user_id' => Auth::user()->id,
        ]);

        return redirect(route('spending-categories.page'));
    }
}
