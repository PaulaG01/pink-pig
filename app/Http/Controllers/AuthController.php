<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::query()
            ->where('email', $request->get('email'))
            ->where('password', md5($request->get('password')))
            ->first();

        if (!$user) {
            return redirect(route('login-page'))->withErrors(['User was not found']);
        }

        Auth::login($user);

        return redirect(route('default-with-auth'));
    }

    public function register(Request $request){
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = User::query()->create([
            'email' => $request->email,
            'password' => md5($request->password),
            'name' => '',
        ]);

        Auth::login($user);
        return redirect(route('default-with-auth'));
    }
}
