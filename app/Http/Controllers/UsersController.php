<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function update(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);

        User::query()->find($request->get('id'))->update([
            'name' => $request->name,
            'email' =>$request->email,
        ]);

        return redirect(route('profile-page'));
    }
}
