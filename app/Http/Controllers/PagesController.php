<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\SpendingCategory;
use App\Models\Transaction;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    private ?DateTime $periodStart = null;
    private ?DateTime $periodEnd = null;

    private string $period = 'month';

    public function main(Request $request)
    {
        $this->retrieveMainRequest($request);
        $spendingCategories = SpendingCategory::mostPopularCategoriesForPeriod(4, $this->periodStart, $this->periodEnd);
        $lastTransactions = Transaction::getLastForPeriod(4);
        $wheelColors = $spendingCategories->pluck('bg_color');
        $wheelPercentages = $spendingCategories->pluck('percentage');
        $periodStart = $this->periodStart;
        $periodEnd = $this->periodEnd;
        $period = $this->period;

        $sources = Auth::user()->accounts;
        $targets = Auth::user()->spendingCategories->merge($sources->where('account_category_id', '!=', 3));

        $totalAccountsBalance = $sources->sum(function (Account $account) {
            return $account->getBalance('byn');
        });

        $spentTransactions = Transaction::getLastForPeriod(null, Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth(), SpendingCategory::class);
        $totalSpent = $spentTransactions->sum('amount');

        $spentTransactionsLastMont = Transaction::getLastForPeriod(null, Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth(), SpendingCategory::class);
        $moreThanLastMonth = $totalSpent - $spentTransactionsLastMont->sum('amount');

        $data = compact(
            'spendingCategories',
            'lastTransactions',
            'wheelColors',
            'wheelPercentages',
            'periodStart',
            'periodEnd',
            'period',
            'sources',
            'targets',
            'totalAccountsBalance',
            'totalSpent',
            'moreThanLastMonth',
        );

        return view('pages.main', $data);
    }

    public function profile()
    {
        $user = Auth::user();

        $data = compact('user');

        return view('pages.profile', $data);
    }

    public function statistics(Request $request)
    {
        $this->retrieveMainRequest($request);

        switch ($this->period) {
            case 'week':
                $carbonPeriods = CarbonPeriod::create($this->periodStart, $this->periodEnd);
                break;
            case 'month':
                $carbonPeriods = CarbonPeriod::create($this->periodStart, $this->periodEnd, '7 days');
                break;
            case 'year':
                $carbonPeriods = CarbonPeriod::create($this->periodStart, $this->periodEnd, '1 month');
                break;
        }

        $periods = [];
        $valuesSpending = [];
        $valuesIncome = [];
        $colorsSpending = [];
        $colorsIncome = [];
        for ($i = 0; $i < $carbonPeriods->count(); $i++) {
            if ($this->period !== 'week') {
                $start = $i === 0 ? $carbonPeriods->current->startOfDay() : $carbonPeriods->current->startOfDay();
                $carbonPeriods->next();
                $end = $carbonPeriods->current ? $carbonPeriods->current->subDays()->endOfDay() : ($this->period === 'month' ? Carbon::now()->endOfMonth() : Carbon::now()->endOfYear());
            } else {
                $start = $carbonPeriods->current->startOfDay();
                $end = $carbonPeriods->current->copy()->endOfDay();
                $carbonPeriods->next();
            }

            $spentTransactions = Transaction::getLastForPeriod(null, $start, $end, SpendingCategory::class);
            $incomeTransactions = Transaction::getLastForPeriod(null, $start, $end, Account::class)->filter(function ($transaction) {
                return $transaction->source()->account_category_id === 3;
            });

            $valuesSpending[] = $spentTransactions->sum('amount');
            $valuesIncome[] = $incomeTransactions->sum('amount');
            $colorsSpending[] = '#EC7063';
            $colorsIncome[] = '#4CB45A';

            $periods[] = $this->period === 'week' ? $start->format('d M,D') : $start->format('d M,D') . ' - ' . $end->format('d M,D');
        }


        $periods = json_encode($periods);
        $valuesSpending = json_encode($valuesSpending);
        $colorsSpending = json_encode($colorsSpending);
        $valuesIncome = json_encode($valuesIncome);
        $colorsIncome = json_encode($colorsIncome);

        $data = compact(
            'periods',
            'valuesSpending',
            'colorsSpending',
            'valuesIncome',
            'colorsIncome',
        );

        return view('pages.statistics', $data);
    }

    private function retrieveMainRequest(Request $request): void
    {
        switch ($request->get('period')) {
            case 'year':
                $this->periodStart = Carbon::now()->startOfYear();
                $this->periodEnd = Carbon::now()->endOfYear();
                $this->period = 'year';
                break;
            case 'week':
                $this->periodStart = Carbon::now()->startOfWeek();
                $this->periodEnd = Carbon::now()->endOfWeek();
                $this->period = 'week';
                break;
            default:
                $this->periodStart = Carbon::now()->startOfMonth();
                $this->periodEnd = Carbon::now()->endOfMonth();
                $this->period = 'month';
                break;
        }
    }
}
