<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountCategory;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountsController extends Controller
{
    public function index()
    {
        $accounts = Account::cardsAndAccounts();
        $savings = Account::savings();
        $incomes = Account::incomes();
        $totalCount = $accounts->count();
        $totalAmount = $accounts->sum(function ($saving) {
            return $saving->getBalance('byn');
        });
        $totalSavingsCount = $savings->count();
        $totalSavingsAmount = $savings->sum(function ($saving) {
            return $saving->getBalance('byn');
        });

        $accountCategories = AccountCategory::all();
        $currencies = Currency::all();

        $data = compact(
            'accounts',
            'savings',
            'totalCount',
            'totalAmount',
            'totalSavingsCount',
            'totalSavingsAmount',
            'accountCategories',
            'currencies',
            'incomes'
        );

        return view('pages.accounts', $data);

    }

    public function create(Request $request)
    {
        Account::query()->create([
            'name' => $request->get('name'),
            'balance' => $request->get('balance'),
            'account_category_id' => $request->get('account_category_id'),
            'currency_id' => $request->get('currency_id'),
            'user_id' => Auth::user()->id,
            'is_main' => $request->get('is_main') ?? 0,
        ]);

        return redirect(route('accounts-page'));
    }

    public function edit(Request $request)
    {
        $account = Account::findOrFail($request->get('id'));
        $account->update([
            'name' => $request->get('name'),
            'balance' => $request->get('balance'),
            'account_category_id' => $request->get('account_category_id'),
            'currency_id' => $request->get('currency_id'),
            'is_main' => $request->get('is_main') ?? 0,
        ]);

        return redirect(route('accounts-page'));
    }
}
