<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Transactionable;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function create(Request $request)
    {
        $sourceData = json_decode($request->source_id);
        $source = $sourceData->type::find($sourceData->id);

        $targetData = json_decode($request->target_id);
        $target = $targetData->type::find($targetData->id);

        $transaction = Transaction::query()->create([
            'amount' => $request->amount,
        ]);

        $transactionable = Transactionable::query()->create([
            'transaction_id' => $transaction->id,
            'source_type' => $sourceData->type,
            'source_id' => $sourceData->id,
            'target_type' => $targetData->type,
            'target_id' => $targetData->id,
        ]);

        return redirect(route('default-with-auth'));
    }
}
