<?php

namespace App\View\Components\blocks;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class transactionsBlock extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public Collection $lastTransactions
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.blocks.transactions-block');
    }
}
