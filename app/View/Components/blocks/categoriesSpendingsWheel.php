<?php

namespace App\View\Components\blocks;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class categoriesSpendingsWheel extends Component
{
    public \Illuminate\Support\Collection|array $categoriesNames;

    public function __construct(
        public Collection $spendingCategories,
        public \Illuminate\Support\Collection|array $wheelColors,
        public \Illuminate\Support\Collection|array $wheelPercentages,
        public ?DateTime $periodStart = null,
        public ?DateTime $periodEnd = null,
        public string $period = 'month',
        public bool $isNeedPeriods = true,
        public bool $addCreateButton = false,
    )
    {
        $this->categoriesNames = $this->spendingCategories->pluck('name');
//        $this->wheelPercentages = $this->wheelPercentages->map(function (int $perc) {
//            return $perc;
//        });

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.blocks.categories-spendings-wheel');
    }
}
