<?php

namespace App\View\Components\blocks;

use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class accountCard extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public Account   $account,
        public Collection $accountCategories,
        public Collection $currencies,

    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.blocks.account-card');
    }
}
