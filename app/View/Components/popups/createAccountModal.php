<?php

namespace App\View\Components\popups;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class createAccountModal extends Component
{

    public function __construct(
        public Collection $accountCategories,
        public Collection $currencies,
    )
    {
        //
    }

    public function render()
    {
        return view('components.popups.account-modal');
    }
}
