<?php

namespace App\View\Components\popups;

use App\Models\Account;
use App\Models\AccountCategory;
use App\Models\Currency;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class accountModal extends Component
{
    public Collection|array $lastTransactions;

    public Collection|array $accountCategories;

    public Collection|array $currencies;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public ?Account   $account = null,
    )
    {
        $this->lastTransactions = $this->account?->transactions(true, 5);

        $this->accountCategories = AccountCategory::all();
        $this->currencies = Currency::all();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.popups.account-modal');
    }
}
