<?php

namespace App\View\Components\popups;

use App\Models\Color;
use App\Models\Icon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class createSpendingCategoryModal extends Component
{
    public Collection $colors;

    public Collection $icons;

    public function __construct()
    {
        $this->colors = Color::all();
        $this->icons = Icon::all();
    }

    public function render()
    {
        return view('components.popups.create-spending-category-modal');
    }
}
