<?php

namespace App\Observers;

use App\Models\Account;
use App\Models\Transactionable;

class TransactionablesObserver
{
    public function created(Transactionable $transactionable)
    {
        $transaction = $transactionable->transaction;

        $transactionTarget = $transaction->target();
        if ($transactionTarget instanceof Account) {
            $transactionTarget->balance += $transaction->amount;
            $transactionTarget->save();
        }

        $transactionSource = $transaction->source();
        $transactionSource->balance -= $transaction->amount;
        $transactionSource->save();
    }

    /**
     * Handle the Transaction "updated" event.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return void
     */
    public function updated(Transaction $transaction)
    {
        //
    }

    public function deleted(Transactionable $transactionable)
    {
        $transaction = $transactionable->transaction;
        $transactionTarget = $transaction->target();
        if ($transactionTarget instanceof Account) {
            $transactionTarget->balance -= $transaction->amount;
            $transactionTarget->save();
        }

        $transactionSource = $transaction->source();
        $transactionSource->balance += $transaction->amount;
        $transactionSource->save();
    }

    public function restored(Transactionable $Transactionable)
    {
        //
    }

    public function forceDeleted(Transactionable $Transactionable)
    {
        //
    }
}
