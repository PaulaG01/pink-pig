<?php

namespace App\Observers;

use App\Models\Account;
use Illuminate\Support\Facades\Auth;

class AccountsObserver
{
    /**
     * Handle the Account "created" event.
     *
     * @param  \App\Models\Account  $account
     * @return void
     */
    public function created(Account $account)
    {
        $mainAccount = Auth::user()->accounts()->where('is_main', 1)->where('id', '<>', $account->id)->first();

        if ($mainAccount && $account->is_main) {
            $mainAccount->is_main = 0;
            $mainAccount->save();
        }
    }

    /**
     * Handle the Account "updated" event.
     *
     * @param  \App\Models\Account  $account
     * @return void
     */
    public function updated(Account $account)
    {
        $mainAccount = Auth::user()->accounts()->where('is_main', 1)->where('id', '<>', $account->id)->first();

        if ($mainAccount && $account->is_main) {
            $mainAccount->is_main = 0;
            $mainAccount->save();
        }
    }

    /**
     * Handle the Account "deleted" event.
     *
     * @param  \App\Models\Account  $account
     * @return void
     */
    public function deleted(Account $account)
    {
        //
    }

    /**
     * Handle the Account "restored" event.
     *
     * @param  \App\Models\Account  $account
     * @return void
     */
    public function restored(Account $account)
    {
        //
    }

    /**
     * Handle the Account "force deleted" event.
     *
     * @param  \App\Models\Account  $account
     * @return void
     */
    public function forceDeleted(Account $account)
    {
        //
    }
}
