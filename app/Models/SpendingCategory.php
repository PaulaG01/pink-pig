<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SpendingCategory extends Model
{
    protected $guarded = [];
    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function icon(): BelongsTo
    {
        return $this->belongsTo(Icon::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function transactions(): MorphToMany
    {
        return $this->morphToMany(
            Transaction::class,
            'target',
            'transactionables',
        );
    }

    public static function getCategoriesTransactionsForPeriod(
        ?DateTime $dateStart = null,
        ?DateTime $dateEnd = null
    ): Collection
    {
        return self::query()
            ->where('user_id', Auth::user()->id)
            ->with('transactions')
            ->whereRelation('transactions', function ($transaction) use ($dateEnd, $dateStart) {
                if (!$dateStart || !$dateEnd) {
                    return $transaction;
                }
                return $transaction->whereBetween('transactions.created_at', [$dateStart, $dateEnd]);
            })->get()->pluck('transactions')->flatten();
    }

    public static function mostPopularCategoriesForPeriod(
        ?int $count,
        ?DateTime $dateStart = null,
        ?DateTime $dateEnd = null,
        bool $includeOther = true,
    ): \Illuminate\Database\Eloquent\Collection|Collection|array
    {
        $categories = self::query()->where('user_id', Auth::user()->id)->get();

        $categories =  $categories->sortByDesc(function (self $category) {
            return $category->transactions->sum('amount');
        });

        if ($count) {
            $categories = $categories->take($count);
        }

        $totalSum = self::getCategoriesTransactionsForPeriod($dateStart, $dateEnd)->sum('amount');
        $mostPopular = $categories->map(function (self $category) use ($totalSum, $dateEnd, $dateStart) {
            $sum = $category
                ->transactions()
                ->when($dateStart && $dateEnd, function (Builder $category) use ($dateEnd, $dateStart) {
                    $category->whereBetween('transactions.created_at', [$dateStart, $dateEnd]);
                })
                ->sum('amount');
            $category->amount = $sum;
            $category->percentage = $totalSum ? round($sum / $totalSum * 100, 2) : 0;
            $category->bg_color = $category->color->hash;
            $category->icon_path = $category->icon->path;

            return $category;
        });

        $otherSum = $totalSum - $mostPopular->sum('amount');

        if (!$includeOther) {
            return $mostPopular->sortByDesc('amount');
        }

        $other = (object) [
            'name' => 'other',
            'color_id' => 1,
            'icon_id' => 1,
            'amount' => $otherSum,
            'percentage' => $totalSum ? round($otherSum / $totalSum * 100, 2) : 100,
            'bg_color' => '#D1C9C8',
            'icon_path' => '',
        ];

        return $mostPopular->push($other)->sortByDesc('amount');
    }
}
