<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    protected $guarded = [];

    public function source(): Account|null
    {
        return $this->morphedByMany(Account::class, 'source', 'transactionables')->first();
    }

    public function target(): Account|SpendingCategory|null
    {
        return $this->targetAccount()->get()->merge($this->targetCategory()->get())->first();
    }

    public static function getLastForPeriod(
        ?int       $count = null,
        ?DateTime $periodStart = null,
        ?DateTime $periodEnd = null,
        ?string $targetType = null,
    ): Collection|array
    {
        return self::query()->when($periodStart && $periodEnd, function (Builder $transaction) use ($periodEnd, $periodStart) {
            $transaction->whereBetween('created_at', [$periodStart, $periodEnd]);
        })
            ->whereHas('sourceRelation', function ($query) use ($targetType){
                $query = $query->where('user_id', Auth::user()->id);

                if ($targetType) {
                    $query->where('target_type',$targetType);
                }

                return $query;
            })
            ->orderByDesc('created_at')
            ->when($count, function ($query) use ($count) {
                $query->limit($count);
            })
            ->get();
    }

    private function targetAccount(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(Account::class, 'target', 'transactionables');
    }

    private function targetCategory()
    {
        return $this->morphedByMany(SpendingCategory::class, 'target', 'transactionables');
    }

    public function sourceRelation()
    {
        return $this->morphedByMany(Account::class, 'source', 'transactionables');
    }

    public function targetRelation()
    {
        return $this->morphedByMany(Account::class, 'target', 'transactionables');
    }
}
