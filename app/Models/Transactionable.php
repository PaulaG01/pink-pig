<?php

namespace App\Models;

use App\Observers\TransactionablesObserver;
use Illuminate\Database\Eloquent\Model;

class Transactionable extends Model
{
    protected static function boot(): void
    {
        parent::boot();

        static::created(function ($transaction) {
            Transactionable::observe(TransactionablesObserver::class);
        });

    }

    protected $guarded = [];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
