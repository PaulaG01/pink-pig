<?php

namespace App\Models;

use App\Observers\AccountsObserver;
use App\Services\Currencies\DailyCurrencies;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        Account::observe(AccountsObserver::class);
    }

    public function getBalance(?string $targetCurrency = 'byn')
    {
        return $this->formatBalance($this->balance, $targetCurrency);
    }

    public function getShortNameAttribute(): string
    {
        return $this->currency->symbol;
    }

    public function getUsdAttribute(): float
    {
        $currency = DailyCurrencies::getFor($this->currency->symbol, 'usd');

        return round($this->balance * $currency->currency, 2);
    }

    public function getEurAttribute(): float
    {
        $currency = DailyCurrencies::getFor($this->currency->symbol, 'eur');

        return round($this->balance * $currency->currency, 2);
    }

    public function getBynAttribute(): float
    {
        $currency = DailyCurrencies::getFor($this->currency->symbol, 'byn');

        return round($this->balance * $currency->currency, 2);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function accountCategory(): BelongsTo
    {
        return $this->belongsTo(AccountCategory::class);
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function transactions(
        bool $groupByCreatedAt = false,
        ?int $count = null,
    ): Collection|array
    {
        $transactions = $this->outgoingTransactions->merge($this->incomingTransactions)->sortByDesc('created_at');
        if ($count) {
            $transactions = $transactions->take($count);
        }

        if ($groupByCreatedAt) {
            $transactions = $transactions->groupBy(function ($item, $key) {
                return $item->created_at->format('Y-m-d');
            })->all();
        }
        return $transactions;
    }

    public function income()
    {
        $income = 0;
        $incomingTransactions = $this->incomingTransactions->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
        foreach ($incomingTransactions as $transaction) {
            $income += $transaction->source()->account_category_id === 3 ? $transaction->amount : 0;
        }

        return $income;
    }

    public function spent()
    {
        $income = 0;
        $outgoingTransactions = $this->outgoingTransactions->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()]);
        foreach ($outgoingTransactions as $transaction) {
            $income += $transaction->target() instanceof SpendingCategory ? $transaction->amount : 0;
        }

        return $income;
    }

    public function outgoingTransactions(): MorphToMany
    {
        return $this->morphToMany(Transaction::class, 'source', 'transactionables')
            ->orderByDesc('transactions.created_at');
    }

    public function incomingTransactions(): MorphToMany
    {
        return $this->morphToMany(Transaction::class, 'target', 'transactionables')
            ->orderByDesc('transactions.created_at');
    }

    public static function cardsAndAccounts(): Collection|array
    {
        return self::addTransactionsLink(self::query()
            ->where('user_id', Auth::user()->id)
            ->where('account_category_id', 1)
            ->orderByDesc('is_main')
            ->orderByDesc('created_at')
            ->get());
    }

    public static function savings(): Collection|array
    {
        return self::addTransactionsLink(self::query()
            ->where('user_id', Auth::user()->id)
            ->where('account_category_id', 2)
            ->orderByDesc('created_at')
            ->get());
    }

    public static function incomes(): Collection
    {
        return self::query()
            ->where('account_category_id', 3)
            ->orderByDesc('created_at')
            ->get();
    }

    private static function addTransactionsLink(Collection $collection)
    {
        return $collection->map(function ($item) {
            $item->linkToTransactions = route('transactions-page', ['type' => 'account', 'id' => $item->id]);
            return $item;
        });
    }

    private function formatBalance(float $balance, string $targetCurrency = 'byn')
    {
        $currency = DailyCurrencies::getFor($this->currency->symbol, $targetCurrency);

        return round($balance * $currency->currency, 2);
    }
}
