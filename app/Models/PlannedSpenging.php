<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PlannedSpenging extends Model
{
    public function spendingCategory(): BelongsTo
    {
        return $this->belongsTo(SpendingCategory::class);
    }
}
