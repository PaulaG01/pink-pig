<?php

use App\Http\Controllers\AccountsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\SpendingCategoriesController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('default-no-auth');

Route::get('/main', [PagesController::class, 'main'])->name('default-with-auth');
Route::get('/profile', [PagesController::class, 'profile'])->name('profile-page');
Route::get('/statistics', [PagesController::class, 'statistics'])->name('statistics-page');

Route::get('login', function () {
    return view('pages.login');
})->name('login-page');

Route::get('register', function () {
    return view('pages.register');
})->name('register-page');

Route::get('transactions', function () {
    dd(1);
//    return view('pages.accounts');
})->name('transactions-page');

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::post('register', [AuthController::class, 'register'])->name('register');

Route::prefix('accounts')->group(function () {
    Route::get('all', [AccountsController::class, 'index'])->name('accounts-page');
    Route::post('create', [AccountsController::class, 'create'])->name('accounts.create');
    Route::post('edit', [AccountsController::class, 'edit'])->name('accounts.edit');
});

Route::prefix('spendingCategories')->group(function () {
    Route::get('all', [SpendingCategoriesController::class, 'index'])->name('spending-categories.page');
    Route::post('create', [SpendingCategoriesController::class, 'create'])->name('spending-categories.create');
});

Route::prefix('transactions')->group(function () {
    Route::post('create', [TransactionsController::class, 'create'])->name('transactions.create');
});

Route::prefix('users')->group(function () {
    Route::post('update', [UsersController::class, 'update'])->name('users.update');
});

