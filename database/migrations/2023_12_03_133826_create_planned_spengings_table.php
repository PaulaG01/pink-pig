<?php

use App\Models\SpendingCategory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlannedSpengingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planned_spengings', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(SpendingCategory::class);
            $table->float('amount')->default(0);
            $table->dateTime('period_start');
            $table->dateTime('period_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planned_spengings');
    }
}
