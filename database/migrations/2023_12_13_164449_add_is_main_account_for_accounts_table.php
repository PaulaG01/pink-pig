<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsMainAccountForAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $isFieldExists = Schema::hasColumn('accounts', 'is_main');
        if (!$isFieldExists) {
            Schema::table('accounts', function (Blueprint $table) {
                $table->boolean('is_main')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $isFieldExists = Schema::hasColumn('accounts', 'is_main');
        if ($isFieldExists) {
            Schema::table('accounts', function (Blueprint $table) {
                $table->dropColumn('is_main');
            });
        }
    }
}
