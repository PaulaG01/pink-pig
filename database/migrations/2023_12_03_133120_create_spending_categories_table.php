<?php

use App\Models\Color;
use App\Models\Icon;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpendingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spending_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignIdFor(Color::class)->default(1);
            $table->foreignIdFor(Icon::class)->default(1);
            $table->foreignIdFor(User::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spending_categories');
    }
}
