FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
    git \
    nano \
    libpq-dev \
    libzip-dev \
    zip \
    unzip \
    wget

RUN apt-get update && apt-get install -y zlib1g-dev libpng-dev libzip-dev\
    && docker-php-ext-install pdo pdo_mysql pdo_pgsql mysqli exif && docker-php-ext-enable mysqli && docker-php-ext-install zip

RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
RUN a2enmod rewrite
RUN service apache2 restart

RUN curl -sS https://getcomposer.org/installer | php -- \
     --install-dir=/usr/local/bin --filename=composer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENV APACHE_DOCUMENT_ROOT /var/www/html
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN docker-php-ext-install gd

