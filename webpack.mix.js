const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', []);

/// PAGES ///
mix.css('resources/css/pages/welcome.css', 'public/css');
mix.css('resources/css/pages/main.css', 'public/css');
mix.css('resources/css/pages/login.css', 'public/css');
mix.css('resources/css/pages/profile.css', 'public/css');
mix.css('resources/css/pages/statistics.css', 'public/css');


/// LAYOUTS ///
mix.css('resources/css/layouts/header.css', 'public/css');
mix.css('resources/css/layouts/footer.css', 'public/css');

/// BLOCKS ///
mix.css('resources/css/blocks/vertical_menu.css', 'public/css');
mix.css('resources/css/blocks/categories_spendings_wheel.css', 'public/css');
mix.css('resources/css/blocks/last_transactions.css', 'public/css');
mix.css('resources/css/blocks/account_card.css', 'public/css');

/// JS ///
mix.js('resources/js/pages/login.js', 'public/js')

/// ITEMS ///
mix.css('resources/css/items/oval.css', 'public/css');

/// POPUPS ///
mix.css('resources/css/popups/create_account.css', 'public/css');
